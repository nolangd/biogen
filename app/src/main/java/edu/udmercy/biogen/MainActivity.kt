package edu.udmercy.biogen

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var firstName = findViewById<EditText>(R.id.editFirstName)
        var lastName = findViewById<EditText>(R.id.editLastName)
        var major = findViewById<EditText>(R.id.editMajor)

        var btn = findViewById<Button>(R.id.button_create)

        // handle button click
        btn.setOnClickListener {
            val sendFromMainActivity = firstName.text.toString() + " " + lastName.text.toString() + " " + major.text.toString()

            val intent = Intent(this@MainActivity, ResultsActivity::class.java)
            intent.putExtra("MainActivityString", sendFromMainActivity)
            startActivity(intent)
        }
    }
}